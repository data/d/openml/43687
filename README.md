# OpenML dataset: Breast-cancer-prediction

https://www.openml.org/d/43687

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset includes data from a random sample of 20,000 digital and 20,000 film-screen mammograms received by women age 60-89 years within the Breast Cancer Surveillance Consortium (BCSC) between January 2005 and December 2008. Some women contribute multiple examinations to the dataset. Data is useful in teaching about data analysis, epidemiological study designs, or statistical methods for binary outcomes or correlated data.
Content
The data set contains 39998 rows and  13 cols. Attributes are described as follows:
  Field Name     **Type (Format) **Description



AgeAtTheTimeOf_Mammography
number
Patient's age in years at time of mammogram




Radiologists_Assessment
string
Radiologist's assessment based on the BI-RADS scale


---
---
---


IsBinaryIndicatorOfCancer_Diagnosis
boolean
Binary indicator of cancer diagnosis within one year of screening mammogram (false= No cancer diagnosis, true= Cancer diagnosis)


---
---
---


ComparisonMammogramFrom_Mammography
string
Comparison mammogram from prior mammography examination available


---
---
---


PatientsBIRADSBreastDensity
string
Patient's BI-RADS breast density as recorded at time of mammogram


---
---
---


FamilyHistoryOfBreastCancer
string
Family history of breast cancer in a first degree relative


---
---
---


CurrentUseOfHormoneTherapy
string
Current use of hormone therapy at time of mammogram


---
---
---


Binary_Indicator
string
Binary indicator of whether the woman had ever received a prior mammogram


---
---
---


HistoryOfBreast_Biopsy
string
Prior history of breast biopsy


---
---
---


IsFilmOrDigitalMammogram
boolean
Film or digital mammogram (true=Digital mammogram, false=Film mammogram)


---
---
---


Cancer_Type
string
Type of cancer


---
---
---



Acknowledgements
We acknowledge the Breast Cancer Surveillance Consortium (BCSC) for making this data set available for research purposes.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43687) of an [OpenML dataset](https://www.openml.org/d/43687). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43687/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43687/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43687/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

